﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMov : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D EnemyRB;
    private float detectionRange = 4.0f;
    public float EnemyImpulse = 0.015f;
    private int PlayerLayerID;
    public Transform raycastPointG;
    private SpriteRenderer mySpriteRenderer;
    public bool facingRight = true;
    // Start is called before the first frame update
    void Start()
    {
        EnemyRB = GetComponent<Rigidbody2D>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerLayerID |= 1 << LayerMask.NameToLayer("Player");
        EnemyRaycast();

    }
    void EnemyRaycast()
    {
        /*
         * Optimised raycast as
         * 1) Only checks for playerDownwardsRaycastDistance = 1 , i.e. only 1 unit distance
         * 2) Only checks for enemy colliders
        */
        RaycastHit2D hit2 = Physics2D.Raycast(raycastPointG.position, Vector2.left, detectionRange, PlayerLayerID);
        if (hit2.collider != null)
        {
            //to check where your raycast is going
            //Debug.DrawRay(raycastPoint.position , Vector2.down , Color.red , 2);
            if (hit2.collider.tag == "Player")
            {
                //Debug.Log("Detected");
                EnemyRB.AddForce(Vector2.left * EnemyImpulse);
                if (mySpriteRenderer != null)
                {
                    // flip the sprite
                    mySpriteRenderer.flipX = true;
                }
                facingRight = false;

            }
            //Sino detecta nada en la direccion cambiar la direccion del raycast
        }
        RaycastHit2D hit3 = Physics2D.Raycast(raycastPointG.position, Vector2.right, detectionRange, PlayerLayerID);
        if (hit3.collider != null)
        {
            //to check where your raycast is going
            //Debug.DrawRay(raycastPoint.position , Vector2.down , Color.red , 2);
            if (hit3.collider.tag == "Player")
            {
                //Debug.Log("Detected");
                EnemyRB.AddForce(Vector2.right * EnemyImpulse);
                if (mySpriteRenderer != null)
                {
                    // flip the sprite
                    mySpriteRenderer.flipX = false;
                }
                facingRight = true;
            }
            //Sino detecta nada en la direccion cambiar la direccion del raycast
        }
    }
}