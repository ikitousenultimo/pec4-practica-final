﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionDeath : MonoBehaviour
{

    public static bool DieDetected;
    private Collider2D parentColl;
    // Start is called before the first frame update
    void Start()
    {
        parentColl = GetComponentInParent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollision2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DieDetected = true;
            parentColl.enabled = false;
        }
        else if(collision.gameObject.CompareTag("Player")!=true)
        {
            parentColl.enabled = true;
        }
    }
}
