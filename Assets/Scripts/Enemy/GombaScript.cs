﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GombaScript : MonoBehaviour
{
    private Animator GombaAnim;
    public bool timetodie;

    public GameObject GoombaOb;
    private BoxCollider2D GombaCol;

    // Start is called before the first frame update
    void Start()
    {
        GombaAnim = GetComponent<Animator>();
        timetodie = EnemyDetectionDeath.DieDetected;
        GombaCol = GoombaOb.GetComponent<BoxCollider2D>();
        //Raycast Detection.

        // Start is called before the first frame update
    }
    // Update is called once per frame
    void Update()
    {
        if(timetodie==true)
        {
            GombaCol.enabled = false;
        }
    }
    private void OnDestroy()
    {
        GombaAnim.SetBool("IsDead", true);
    }
}

