﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Rigidbody2D gombaRB;
    private float detectionRange=4.0f;
    private float gombaImpulse = 0.015f;
    private int PlayerLayerID;
    public Transform raycastPointG;
    // Start is called before the first frame update
    void Start()
    {
        gombaRB = GetComponent<Rigidbody2D>();
}

    // Update is called once per frame
    void Update()
    {
        PlayerLayerID |= 1 << LayerMask.NameToLayer("Player");
        EnemyRaycast();

    }
    void EnemyRaycast()
    {
        /*
         * Optimised raycast as
         * 1) Only checks for playerDownwardsRaycastDistance = 1 , i.e. only 1 unit distance
         * 2) Only checks for enemy colliders
        */
        RaycastHit2D hit2 = Physics2D.Raycast(raycastPointG.position, Vector2.left, detectionRange, PlayerLayerID );
        if (hit2.collider != null)
        {
            //to check where your raycast is going
            //Debug.DrawRay(raycastPoint.position , Vector2.down , Color.red , 2);
            if (hit2.collider.tag == "Player")
            {
                //Debug.Log("Detected");
                gombaRB.AddForce(Vector2.left * gombaImpulse);
            }
            //Sino detecta nada en la direccion cambiar la direccion del raycast
        }
        RaycastHit2D hit3 = Physics2D.Raycast(raycastPointG.position, Vector2.right, detectionRange, PlayerLayerID);
        if (hit3.collider != null)
        {
            //to check where your raycast is going
            //Debug.DrawRay(raycastPoint.position , Vector2.down , Color.red , 2);
            if (hit3.collider.tag == "Player")
            {
                //Debug.Log("Detected");
                gombaRB.AddForce(Vector2.right * gombaImpulse);
            }
            //Sino detecta nada en la direccion cambiar la direccion del raycast
        }
    }
}
