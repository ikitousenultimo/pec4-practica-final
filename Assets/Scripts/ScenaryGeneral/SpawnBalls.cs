﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBalls : MonoBehaviour
{
    public GameObject[] Balls;
    public Transform SpawnPoint;
    private int i;
    private float Lx;
    private Vector3 PositionBall;
    // Start is called before the first frame update
    void Start()
    {
            InvokeRepeating("Instanceitems", Time.deltaTime, 2.0f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        i = Random.Range(0, Balls.Length);
        Lx = Random.Range(-3.5f, 3.5f);
        PositionBall = new Vector3(Lx, Balls[i].transform.position.y,Balls[i].transform.position.z);
    }
    public void Instanceitems()
    {
        Instantiate(Balls[i], PositionBall, SpawnPoint.rotation);
    }
}
