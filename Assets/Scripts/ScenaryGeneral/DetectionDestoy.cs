﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionDestoy : MonoBehaviour
{
    public static bool DestroyIt;
    public GameObject SpecialWall;
    // Start is called before the first frame update
    void Start()
    {
        DestroyIt = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnDestroy()
    {
        DestroyIt = true;
        Destroy(SpecialWall.gameObject);
    }
}
