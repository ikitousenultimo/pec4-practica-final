﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsMovement : MonoBehaviour
{
    public float speed;
    public bool YesTimeToBack;
    // Start is called before the first frame update
    void Start()
    {
        YesTimeToBack = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.position.z>-75)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
        if(this.transform.position.z<=-75)
        {
            this.transform.position = new Vector3 (transform.position.x,transform.position.y,-75.0f);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                backtoMenu();
            }
        }
    }
    void backtoMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
