﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private PlayerController PlayerControllerScript;
    private FollowPlayer FollowPlayerScript;

    public GameObject PanelGameOver;
    public GameObject CameraM;

    public GameObject Souls;


    public Transform SpawnPointPlayer;
    public GameObject Player2;

    public GameObject Player;


    private bool RestartTime;
    public bool PauseG;


    public List<GameObject> Entities;
    public List<Transform> EnemiesSpawn;
    public int restoflife;
    // Start is called before the first frame update
    void Start()
    {
        PanelGameOver.SetActive(false);
        Player.gameObject.SetActive(true);

        AllSpawns();
        GameStart();
    }

    // Update is called once per frame
    void Update()
    {
        //Condiciones de Perder
        if (PlayerController.Died == true)
        {
            GameOver();
        }
        if (Input.GetKeyDown(KeyCode.P) && PauseG == false)
        {
            Pausefunction();
            PauseG = true;
        }
    }
    void GameOver()
    {
        Player.SetActive(false);
        CountdownCoroutine();
        PanelGameOver.SetActive(true);
        this.gameObject.SetActive(false);
    }
    void GameStart()
    {
        PlayerControllerScript = GetComponent<PlayerController>();
        FollowPlayerScript = GetComponent<FollowPlayer>();


        PanelGameOver.SetActive(false);
        Player.isStatic = false;

        restoflife = 3;
    }
    void AllSpawns()
    {
        Instantiate(Player2, SpawnPointPlayer.position, Quaternion.identity);
        Player.gameObject.SetActive(true);
        for (int i = 0; i < 12; ++i)
        {
            Instantiate(Entities[i], EnemiesSpawn[i].transform.position, Quaternion.identity);
        }
    }
    void PlayerWins()
    {
        Player.isStatic = true;
        PanelGameOver.SetActive(true);
    }
    //Cuenta atras para la aparicion de la pantalla de transicion.
    IEnumerator CountdownCoroutine()
    {
        yield return new WaitForSecondsRealtime(1);
        RestartTime = true;
    }
    public void PlayerSpawn()
    {
        Instantiate(Player2, SpawnPointPlayer.position, Quaternion.identity);
    }
    public void Pausefunction()
    {
        Time.timeScale = 0; //pauses the current scene 
        //Crear la pantalla de pausa.
        SceneManager.LoadScene("Pause",LoadSceneMode.Additive); //loads your desired other scene
    }
}