﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public bool facingRight = true;         // For determining which way the player is currently facing.
    [HideInInspector]
    public bool jump;               // Condition for whether the player should jump.
    public GameObject Player;

    public float moveForce;   // Amount of force added to move the player left and right.
    public float horizontalInput;
    //Cambio del Sprite para Giros
    private SpriteRenderer mySpriteRenderer;

    public float Speed = 5f;               // The fastest the player can travel in the x axis.
    public AudioClip[] AudioClips;          // Array of clips for when the player jumps.
    public AudioSource audioSource;
    public float jumpForce = 1000f;         // Amount of force added when the player jumps.

    public bool grounded = true;          // Whether or not the player is grounded.
    public bool attacking;        // For CD in attack
    public bool shielding;        // For CD in Shielding
    public string NextScene;
    //Animaciones
    private Animator anim;		// Reference to the player's animator component.
    //Cambiado para usar el Sprite Renderer en X
    //private Transform pivot;                //Reference to the player´s transform.

    // Variables para el GameManager.
    public static bool Died;
    public static int CountedCoins;
    public bool PowerUp;
    public static bool gameOver;
    public int NSpawn = 0;
    //Tiempo de invulnerabilidad
    public bool isBack;

    //RayCast Si el jugador consigue un PowerUp (Pensado para futuro)
    //Referencias a otros scripts
    //Raycast para el salto contra los enemigos
    public Transform raycastPoint;
    //Distancia a que el personaje detecta al gomba o enemigo debajo de el. Dejado aqui para PowerUps en el futuro.
    [SerializeField]
    private float playerDownwardsRaycastDistance = 0.02f;
    [SerializeField]
    private int upwardsForceOnPlayer = 200;
    private Rigidbody2D playerRigidBody2D;
    //Para el Raycast y deteccion del enemigo debajo del personaje.
    private int enemyLayerID;
    private void Awake()
    {
        //para los giros del sprite
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Start()
    {
        //SetUp del Script.
        PlayerStartUP();
    }
    void Update()
    {
        MoveSetPlayer();
        
    } 
    void FixedUpdate()
    {
        //Para el powerUp, a futuro si.
        PlayerRaycast();
    }
    void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.tag == "Ground")
        {
            grounded = true;
        }
        else if (collider.gameObject.tag == "EndT")
        {
            SceneManager.LoadScene("MainMenu");
        }
        else if (collider.gameObject.tag == "NormalEnd")
        {
            SceneManager.LoadScene(NextScene);
        }
        //muerte por colision contra enemigos si el jugador no esta atacando.
        if (collider.gameObject.CompareTag("Enemy") && shielding == false && attacking==false)
        {
            gameOver = true;
            Died = true;
        }
        //si el jugador ataca detruye el otro gameObject
        if (collider.gameObject.CompareTag("Enemy") && attacking == true)
        {
            Destroy(collider.gameObject);
        }
        //Si el jugador se esta protegiendo el enemigo rebota, en direccion contraria al jugador.
        if(collider.gameObject.CompareTag("Enemy")&& shielding==true)
        {
            collider.gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-horizontalInput, 0), ForceMode2D.Impulse);
        }
    }
    void MoveSetPlayer()
    {

        {
            //Entrada principal de movimiento via Axis
            horizontalInput = Input.GetAxis("Horizontal");
            if (Input.GetKeyDown(KeyCode.Space) && grounded == true)
            {
                //Si el jugador presiona espacio y esta en el suelo-> Se produce animacion de salto se desactiva el check del suelo, el jugador salta.
                anim.SetBool("Jumping", true);
                grounded = false;
                //audioSource.PlayOneShot(AudioClips[0], 0.7F);
                Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
            }
            //Movimientos horizontales en referencia a la direccion en el que el jugador quiere dirigirse ademas de giro del sprite.
            else if (horizontalInput > 0 && grounded == true)
            {
                Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontalInput, 0f)*Speed);
                anim.SetBool("Walking", true);
                //Giro del sprite
                // tilt -= 1.0f;
                if (mySpriteRenderer != null)
                {
                    // flip the sprite
                    mySpriteRenderer.flipX = false;
                }
                facingRight = true;
                // tilt = Mathf.Clamp(tilt, 0, 180);
                // pivot.rotation = Quaternion.Euler(0, facingRight ? tilt : -tilt, 0);

            }
            else if (horizontalInput < 0 && grounded == true)
            {
                Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontalInput, 0f)*Speed);
                //Animacion de correr
                anim.SetBool("Walking", true);
                //Giro del Sprite
                // tilt += 1.0f;
                if (mySpriteRenderer != null)
                {
                    // flip the sprite
                    mySpriteRenderer.flipX = true;
                }
                facingRight = false;
                //tilt = Mathf.Clamp(tilt, 0, 180);
                //pivot.rotation = Quaternion.Euler(0, facingRight ? tilt : -tilt, 0);
            }
            else if(Input.GetMouseButtonDown(1) && attacking==false)
            {
                anim.SetBool("Shield", true);
                shielding = true;
            }
            else if(Input.GetMouseButtonUp(1))
            {
                shielding = false;
                anim.SetBool("Shield",false);
            }
            if(Input.GetMouseButtonDown(0) && shielding==false)
            {
                anim.SetBool("Attack", true);
                attacking = true;
               //attack
            }
            else if (Input.GetMouseButtonUp(0))
            {
                anim.SetBool("Attack", false);
                attacking = false;
            }
            else if (!Input.anyKey && grounded == true)
            {
                //Si el jugador no toca ninguna tecla y esta en el suelo, se desactivan los bool para las animaciones correspondientes.
                anim.SetBool("Jumping", false);
                anim.SetBool("Walking", false);
                anim.SetBool("Attack", false);
                anim.SetBool("Shield", false);
            }
        }

    }
    void PlayerRaycast()
    {
        /*
         * Optimised raycast as
         * 1) Only checks for playerDownwardsRaycastDistance = 1 , i.e. only 1 unit distance
         * 2) Only checks for enemy colliders
        */
        RaycastHit2D hit = Physics2D.Raycast(raycastPoint.position, Vector2.down, playerDownwardsRaycastDistance, enemyLayerID);

        if (hit.collider != null)
        {
            //to check where your raycast is going
            //Debug.DrawRay(raycastPoint.position , Vector2.down , Color.red , 2);
            if (hit.collider.tag == "Enemy")
            {
                Debug.Log("Eyyy");
                playerRigidBody2D.AddForce(Vector2.up * upwardsForceOnPlayer);
                Destroy(hit.collider.gameObject);
            }
        }
    }
    void PlayerStartUP()
    {
        //Set Start el personaje 
        CountedCoins = 0000;
        NSpawn = 0;
        gameOver = false;
        PowerUp = false;
        isBack = true;
        Died = false;
        //Referencia cruzada con el GameManager
        //Referencia al Audio
        audioSource = GetComponent<AudioSource>();

        //Referencias al animator para el control de animaciones.
        anim = this.GetComponent<Animator>();
        //Referencia al transform del player para el cambio de plano al cambiar de direccion.
        //pivot = Player.transform;
        //Referencia al layer de los enemigos para que durante el salto si es un enemigo dentro del layer sea detectado y el jugador salte de rebote.
        enemyLayerID |= 1 << LayerMask.NameToLayer("Enemy");
        playerRigidBody2D = this.GetComponent<Rigidbody2D>();
    }
    IEnumerator Invulnerabilitytime()
    {
        yield return new WaitForSecondsRealtime(2);
        isBack = true;

    }
    IEnumerator RespawnTime()
    {
        yield return new WaitForSecondsRealtime(1);
        Died = false;

    }
    IEnumerator AttackCD()
    {
        yield return new WaitForEndOfFrame();
        anim.SetBool("Attack", false);
        attacking = false;
    }
    IEnumerator ShieldCD()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        shielding = false;
    }
}
