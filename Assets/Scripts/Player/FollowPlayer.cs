﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 offset;          // The offset at which the Health Bar follows the player.
    public float RightLimit;
    public float LeftLimit;
    public GameObject Camera;
    //NEW
    private GameObject player;
    // Reference to the player.


    void Awake()
    {
        // Setting up the reference.
        // NEW
        // player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Camera.transform.position = player.transform.position + offset;
        // Set the position to the player's position with the offset.
    }
}
