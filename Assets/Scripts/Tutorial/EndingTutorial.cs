﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingTutorial : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollision2D(Collision collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
