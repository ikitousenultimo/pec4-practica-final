﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
    public GameObject Sprite1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void  StartTheGame()
    {
        Sprite1.gameObject.SetActive(false);
        StartCoroutine(CDfortransition());
    }
    IEnumerator CDfortransition()
    {
        yield return new WaitForSecondsRealtime(1);
        SceneManager.LoadScene("Game1");
    }
}
