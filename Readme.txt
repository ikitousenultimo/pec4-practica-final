1.Primero he creado los sprites usando Photoshop e illustrator
2.He decidido un nombre para el juego.
3.He buscado una presentacion adecuada para el mismo.
4.He dividido el juego por escenas.
5.He creado el Menu principal/El menu de Pausa y El game over.
6. El menu de pausa y el Menu principal son escenarios aislados
7. El game over es un panel a menudo el mismo panel da problemas si se inicia el juego varias veces seguidas.
8. Desde el menu de pausa se puede volver a iniciar al juego principal.
9. He montado el escenario tutorial en base a sprites modificados por mi.
10. El juego esta pensado para tener unas mecanicas mas complejas a futuro.
11. Por eso mismo, hay restos de codigo dentro del mismo con la intencion de ampliar el gameplay. Sin embargo esto no ofrece
problemas a la hora de jugar.
12. Objetivos del juego: llegar a lo mas profundo del escenario sin morir.
13. El tutorial es autoexplicativo.
14. El menu es interactivo, sin embargo se me desproporciona al escalarlo.
15. El fondo del escenario principal es transparente que combinado con un color de camara en negro da la sensacion de estar en una caverna.
16.La caida de esferas es ambiental.
17. A futuro quiero convertir este juego en un rogue litle aleatorio donde los escenarios se monten de forma automatica.
18. Ademas se tienen pensados distintos tipos de enemigos y objetos como se puede observar en la carpeta de Sprites.
19. Espero que le guste el resultado. Un saludo. Felices fiestas.